import sys
import multiprocessing

if sys.platform.startswith("linux"):
    multiprocessing.set_start_method("spawn", force=True)

import shutil
import subprocess

import os
from datetime import timedelta
import json
import argparse
from pathlib import Path

import torch
from torch import nn, optim
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torchvision.utils import make_grid
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint

from lib.model import Discriminator, UNet, Discriminator
import lib.dataset as dataset
from lib.diffusion import GaussianDiffusion, make_beta_schedule

import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt


class obj(object):
    def __init__(self, d):
        for a, b in d.items():
            if isinstance(b, (list, tuple)):
                setattr(self, a, [obj(x) if isinstance(x, dict) else x for x in b])
            else:
                setattr(self, a, obj(b) if isinstance(b, dict) else b)


def accumulate(model1, model2, decay=0.9999):
    par1 = dict(model1.named_parameters())
    par2 = dict(model2.named_parameters())

    for k in par1.keys():
        par1[k].data.mul_(decay).add_(par2[k].data, alpha=1 - decay)


def samples_fn(model, diffusion, shape):
    samples = diffusion.p_sample_loop(model=model, shape=shape, noise_fn=torch.randn)
    return {"samples": (samples + 1) / 2}


def progressive_samples_fn(model, diffusion, shape, device, include_x0_pred_freq=50):
    samples, progressive_samples = diffusion.p_sample_loop_progressive(
        model=model,
        shape=shape,
        noise_fn=torch.randn,
        device=device,
        include_x0_pred_freq=include_x0_pred_freq,
    )
    return {
        "samples": (samples + 1) / 2,
        "progressive_samples": (progressive_samples + 1) / 2,
    }


def bpd_fn(model, diffusion, x):
    total_bpd_b, terms_bpd_bt, prior_bpd_b, mse_bt = diffusion.calc_bpd_loop(
        model=model, x_0=x, clip_denoised=True
    )

    return {
        "total_bpd": total_bpd_b,
        "terms_bpd": terms_bpd_bt,
        "prior_bpd": prior_bpd_b,
        "mse": mse_bt,
    }


def validate(val_loader, model, diffusion):
    model.eval()
    bpd = []
    mse = []
    with torch.no_grad():
        for i, (x, y) in enumerate(iter(val_loader)):
            x = x
            metrics = bpd_fn(model, diffusion, x)

            bpd.append(metrics["total_bpd"].view(-1, 1))
            mse.append(metrics["mse"].view(-1, 1))

        bpd = torch.cat(bpd, dim=0).mean()
        mse = torch.cat(mse, dim=0).mean()

    return bpd, mse


class DDP(pl.LightningModule):
    def __init__(self, conf):
        super().__init__()

        self.conf = conf
        self.save_hyperparameters()

        self.model = UNet(
            self.conf.model.in_channel,
            self.conf.model.channel,
            channel_multiplier=self.conf.model.channel_multiplier,
            n_res_blocks=self.conf.model.n_res_blocks,
            attn_strides=self.conf.model.attn_strides,
            dropout=self.conf.model.dropout,
            fold=self.conf.model.fold,
        )

        self.ema = UNet(
            self.conf.model.in_channel,
            self.conf.model.channel,
            channel_multiplier=self.conf.model.channel_multiplier,
            n_res_blocks=self.conf.model.n_res_blocks,
            attn_strides=self.conf.model.attn_strides,
            dropout=self.conf.model.dropout,
            fold=self.conf.model.fold,
        )
        # if hasattr(self.conf, "discriminator") and self.conf.discriminator:
        #     self.enable_adversarial_training = True
        #     self.discriminator = nn.discriminator(
        #         self.conf.model.in_channel,
        #         self.conf.discriminator.channel,
        #         dropout=self.conf.model.dropout,
        #         fold=self.conf.model.fold,
        #     )
        # else:
        #     self.enable_adversarial_training = False

        self.betas = make_beta_schedule(
            schedule=self.conf.model.schedule.type,
            start=self.conf.model.schedule.beta_start,
            end=self.conf.model.schedule.beta_end,
            n_timestep=self.conf.model.schedule.n_timestep,
        )

        self.diffusion = GaussianDiffusion(
            betas=self.betas,
            model_mean_type=self.conf.model.mean_type,
            model_var_type=self.conf.model.var_type,
            loss_type=self.conf.model.loss_type,
        )

    def setup(self, stage):
        self.train_set, self.valid_set = dataset.get_train_data(self.conf)

    def forward(self, x):

        return self.diffusion.p_sample_loop(self.model, x.shape)

    def configure_optimizers(self):

        if self.conf.training.optimizer.type == "adam":
            optimizer = optim.Adam(
                self.model.parameters(), lr=self.conf.training.optimizer.lr
            )
        else:
            raise NotImplementedError

        return optimizer

    def training_step(self, batch, batch_nb):
        step = self.current_epoch * self.conf.training.dataloader.batch_size + batch_nb

        img, _ = batch
        time = (torch.rand(img.shape[0]) * 1000).type(torch.int64).to(img.device)
        loss = self.diffusion.training_losses(self.model, img, time).mean()

        accumulate(
            self.ema,
            self.model.module
            if isinstance(self.model, nn.DataParallel)
            else self.model,
            0.9999,
        )

        tensorboard_logs = {"train_loss": loss}

        self.logger.log_metrics(tensorboard_logs, step)

        return {"loss": loss, "log": tensorboard_logs}

    def train_dataloader(self):

        train_loader = DataLoader(
            self.train_set,
            batch_size=self.conf.training.dataloader.batch_size,
            shuffle=True,
            num_workers=self.conf.training.dataloader.num_workers,
            pin_memory=True,
            drop_last=self.conf.training.dataloader.drop_last,
        )

        return train_loader

    def validation_step(self, batch, batch_nb):

        img, _ = batch
        time = (torch.rand(img.shape[0]) * 1000).type(torch.int64).to(img.device)
        loss = self.diffusion.training_losses(self.ema, img, time).mean()

        return {"val_loss": loss}

    def validation_epoch_end(self, outputs):

        avg_loss = torch.stack([x["val_loss"] for x in outputs]).mean()
        tensorboard_logs = {"val_loss": avg_loss}

        shape = (16, 3, self.conf.dataset.resolution, self.conf.dataset.resolution)
        sample = progressive_samples_fn(
            self.ema, self.diffusion, shape, device="cuda" if self.on_gpu else "cpu"
        )

        grid = make_grid(sample["samples"], nrow=4)
        self.logger.experiment.add_image(f"generated_images", grid, self.current_epoch)

        grid = make_grid(
            sample["progressive_samples"].reshape(
                -1, 3, self.conf.dataset.resolution, self.conf.dataset.resolution
            ),
            nrow=20,
        )
        self.logger.experiment.add_image(
            f"progressive_generated_images", grid, self.current_epoch
        )

        self.logger.log_metrics(tensorboard_logs, self.current_epoch)

        return {"val_loss": avg_loss, "log": tensorboard_logs}

    def val_dataloader(self):
        valid_loader = DataLoader(
            self.valid_set,
            batch_size=self.conf.validation.dataloader.batch_size,
            shuffle=False,
            num_workers=self.conf.validation.dataloader.num_workers,
            pin_memory=True,
            drop_last=self.conf.validation.dataloader.drop_last,
        )

        return valid_loader


class DDPGAN(DDP):
    def __init__(self, conf):
        super().__init__(conf)
        self.discriminator = Discriminator(
            self.conf.model.in_channel,
            self.conf.discriminator.channel,
            self.conf.discriminator.size_downsample,
            dropout=self.conf.model.dropout,
            fold=self.conf.model.fold,
        )

    def configure_optimizers(self):

        if self.conf.training.optimizer.type == "adam":
            optimizer = optim.Adam(
                self.model.parameters(), lr=self.conf.training.optimizer.lr
            )
            optimizer2 = optim.Adam(
                self.discriminator.parameters(),
                lr=self.conf.training.optimizer.lr * 1e-1,
            )

        else:
            raise NotImplementedError
        return [optimizer, optimizer2], []

    def training_step(self, batch, batch_nb, optimizer_idx):
        step = self.current_epoch * self.conf.training.dataloader.batch_size + batch_nb

        img, _ = batch
        if optimizer_idx == 0:
            time = (torch.rand(img.shape[0]) * 1000).type(torch.int64).to(img.device)
            losses, fake_img = self.diffusion.training_losses(
                self.model, img, time, return_pred_x0=True
            )

            generator_loss = losses.mean()
            disc_loss = F.softplus(-self.discriminator(fake_img)).mean()
            loss = generator_loss + disc_loss

            self.fake_img = fake_img.detach()

            accumulate(
                self.ema,
                self.model.module
                if isinstance(self.model, nn.DataParallel)
                else self.model,
                0.9999,
            )

            tb_partial_loss = {
                "train_reconstruct_loss": generator_loss,
                "train_disc_loss": disc_loss,
            }

            self.logger.log_metrics(tb_partial_loss, step)
            tb_final_logs = {"train_loss": loss}
            self.logger.log_metrics(tb_final_logs, step)
            if batch_nb % 1000 == 0:
                grid = make_grid(
                    fake_img.reshape(
                        -1,
                        3,
                        self.conf.dataset.resolution,
                        self.conf.dataset.resolution,
                    ),
                    nrow=4,
                )
                self.logger.experiment.add_image("train_diffusion", grid, step)

            return {"loss": loss, "log": dict(**tb_partial_loss, **tb_final_logs)}
        elif optimizer_idx == 1:
            loss = (
                F.softplus(self.discriminator(img))
                + F.softplus(-self.discriminator(self.fake_img))
            ).mean()

            tb_partial_loss = {"train_adv_loss": loss}

            self.logger.log_metrics(tb_partial_loss, step)

            return {"loss": loss, "log": tb_partial_loss}


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--tensorboard", default=False, action="store_true")
    parser.add_argument(
        "--train", action="store_true", default=False, help="Training or evaluation?"
    )
    parser.add_argument("--config", type=str, required=True, help="Path to config.")

    # Training specific args
    parser.add_argument(
        "--ckpt_dir",
        type=str,
        default="ckpts",
        help="Path to folder to save checkpoints.",
    )
    parser.add_argument(
        "--ckpt_freq",
        type=int,
        default=20,
        help="Frequency of saving the model (in epoch).",
    )
    parser.add_argument(
        "--n_gpu", type=int, default=1, help="Number of available GPUs."
    )
    parser.add_argument("--resume", action="store_true", default=False)

    # Eval specific args
    parser.add_argument(
        "--model_dir",
        type=str,
        default="final/cifar10.ckpt",
        help="Path to model for loading.",
    )
    parser.add_argument(
        "--sample_dir",
        type=str,
        default="samples",
        help="Path to save generated samples.",
    )
    parser.add_argument(
        "--prog_sample_freq",
        type=int,
        default=200,
        help="Progressive sample frequency.",
    )
    parser.add_argument(
        "--n_samples",
        type=int,
        default=20,
        help="Number of generated samples in evaluation.",
    )

    args = parser.parse_args()

    path_to_config = args.config
    with open(path_to_config, "r") as f:
        conf = json.load(f)

    conf = obj(conf)
    if hasattr(conf, "discriminator"):
        denoising_diffusion_model = DDPGAN(conf)
    else:
        denoising_diffusion_model = DDP(conf)

    if args.tensorboard:
        found = shutil.which("tensorboard")
        if found is None:
            raise ValueError("Tensorboard not found. Please install tensorboard.")
        logdir = str((Path(__file__).parent / "lightning_logs").resolve())
        cmd = ([found, "--logdir", logdir, "--port", "8080", "--bind_all"],)
        subprocess.Popen(
            cmd,
            shell=True,
        )

    if args.train:
        checkpoint_dir = Path(args.ckpt_dir)
        checkpoint_dir.mkdir(parents=True, exist_ok=True)
        checkpoint_callback = ModelCheckpoint(
            dirpath=args.ckpt_dir,
            filename="ddp_{epoch:02d}-{val_loss:.2f}",
            monitor="val_loss",
            verbose=False,
            save_last=True,
            save_top_k=-1,
            save_weights_only=True,
            # mode="auto",
            every_n_epochs=args.ckpt_freq,
        )
        last_training_checkpoint_callback = ModelCheckpoint(
            dirpath=args.ckpt_dir,
            filename="ddp_last.ckpt",
            save_last=False,
            save_weights_only=False,
            train_time_interval=timedelta(minutes=30),
        )

        if args.resume:
            resume_from_checkpoint = str(checkpoint_dir / "ddp_last.ckpt")
        else:
            resume_from_checkpoint = None

        trainer = pl.Trainer(
            fast_dev_run=False,
            gpus=args.n_gpu,
            # strategy="deepspeed",
            # enable_model_summary=False,
            # enable_progress_bar=False,
            # replace_sampler_ddp=False,
            detect_anomaly=True,
            max_steps=conf.training.n_iter,
            precision=conf.model.precision,
            gradient_clip_val=1.0,
            progress_bar_refresh_rate=20,
            callbacks=[checkpoint_callback, last_training_checkpoint_callback],
            resume_from_checkpoint=resume_from_checkpoint,
        )

        trainer.fit(denoising_diffusion_model)

    else:

        denoising_diffusion_model.cuda()
        state_dict = torch.load(args.model_dir)
        denoising_diffusion_model.load_state_dict(state_dict["state_dict"])
        denoising_diffusion_model.eval()

        sample = progressive_samples_fn(
            denoising_diffusion_model.ema,
            denoising_diffusion_model.diffusion,
            (args.n_samples, 3, conf.dataset.resolution, conf.dataset.resolution),
            device="cuda",
            include_x0_pred_freq=args.prog_sample_freq,
        )

        if not os.path.exists(args.sample_dir):
            Path(args.sample_dir).mkdir(exist_ok=True, parents=True)

        for i in range(args.n_samples):

            img = sample["samples"][i]
            plt.imsave(
                os.path.join(args.sample_dir, f"sample_{i}.png"),
                img.cpu().numpy().transpose(1, 2, 0),
            )

            img = sample["progressive_samples"][i]
            img = make_grid(img, nrow=args.prog_sample_freq)
            plt.imsave(
                os.path.join(args.sample_dir, f"prog_sample_{i}.png"),
                img.cpu().numpy().transpose(1, 2, 0),
            )
