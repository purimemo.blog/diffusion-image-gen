# FROM nvidia/cuda:11.2.0-cudnn8-devel-ubuntu20.04
# FROM nvidia/cuda:11.4.3-cudnn8-devel-ubuntu20.04
FROM nvidia/cuda:11.6.1-cudnn8-devel-ubuntu20.04

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV MINIO_HOST=localhost:9000
ENV MINIO_SECURE=0
ENV MINIO_CACHE_DIR=
ENV PUBLIC_KEY=

RUN apt-get update \
  && apt-get install -yqq --no-install-recommends \
  python3 python3-pip python3-venv python3-pil\
  zlib1g-dev libjpeg8 libjpeg8-dev libfreetype6 libfreetype6-dev\
  openssh-server vim git\
  && pip3 install numpy matplotlib pytorch-lightning scikit-learn imageio minio tensorboard\
  && pip3 install --extra-index-url https://download.pytorch.org/whl/cu116/ torch torchvision\
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*\
  && rm -rf ~/.cache/pip


COPY ./docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

CMD [ "/docker-entrypoint.sh" ]