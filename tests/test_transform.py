import pytest
import torch
from lib.dataset import FixedDepthTransform


@pytest.mark.parametrize("depth", [1, 2, 3, 4, 5])
def test_fixed_depth_transform_increase_depth(depth):
    img = torch.zeros((depth, 32, 32))
    transformed = FixedDepthTransform()(img)
    assert transformed.shape[0] == 3
    assert transformed.shape[1:2] == img.shape[1:2]
