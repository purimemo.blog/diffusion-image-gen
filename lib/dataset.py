from io import BytesIO
from typing import Any, List
from sklearn.model_selection import train_test_split
import torch
import torchvision.transforms as T
import torchvision.datasets
import numpy as np
from torch.utils.data import Dataset, Subset
from pathlib import Path
import json
import imageio
import minio
import torchvision.transforms.functional as TF
from tqdm.auto import tqdm
from PIL import Image, ImageFile
import os

ImageFile.LOAD_TRUNCATED_IMAGES = True

MINIO_HOST = os.getenv("MINIO_HOST", "127.0.0.1:9000")
MINIO_SECURE = bool(int(os.getenv("MINIO_SECURE", "0")))
MINIO_CACHE_DIR = os.getenv("MINIO_CACHE_DIR", None)
MINIO_CACHE_ON_MEMORY = bool(os.getenv("MINIO_CACHE_ON_MEMORY", False))


class AdultImageLoader(Dataset):
    train_object_path = Path("./index/cache/train.json")
    valid_object_path = Path("./index/cache/valid.json")
    # test_index = Path("./index/test.json")

    def __init__(self, split: str = None, transform: Any = None):
        self.cache_dir = Path(MINIO_CACHE_DIR) if MINIO_CACHE_DIR is not None else None
        self.memory_cache = {} if MINIO_CACHE_ON_MEMORY else None
        if split is None:
            self.objects = self._load_objects()
        elif split in ("train", "valid"):
            if split == "train" and self.train_object_path.exists():
                self.objects = json.loads(self.train_object_path.read_text())
            elif split == "valid" and self.valid_object_path.exists():
                self.objects = json.loads(self.valid_object_path.read_text())
            else:
                train_objects, valid_objects = train_test_split(
                    self._load_objects(), test_size=10000
                )
                self.train_object_path.parent.mkdir(exist_ok=True, parents=True)
                self.valid_object_path.parent.mkdir(exist_ok=True, parents=True)
                self.train_object_path.write_text(json.dumps(train_objects))
                self.valid_object_path.write_text(json.dumps(valid_objects))
                if split == "train":
                    self.objects = train_objects
                elif split == "valid":
                    self.objects = valid_objects
        else:
            raise ValueError(f"Unknown split: {split}")

        self.transform = transform

    def _load_objects_v0(self) -> List[Any]:
        client = minio.Minio(
            MINIO_HOST,
            access_key="root",
            secret_key="hurh329iu43joi2njfds902139",
            secure=MINIO_SECURE,
        )
        objects = []
        for bucket in client.list_buckets():
            bucket_name = bucket.name
            objects.extend(
                (bucket_name, obj.object_name)
                for obj in client.list_objects(bucket_name, recursive=True)
            )
        print(f"Loaded: {len(objects)}")
        return objects

    def _load_objects_v1(self) -> List[Any]:
        client = minio.Minio(
            MINIO_HOST,
            access_key="root",
            secret_key="hurh329iu43joi2njfds902139",
            secure=MINIO_SECURE,
        )

        objects = []
        for file in Path("./index").glob("*.json"):
            configs = json.loads(file.read_text())
            for config in tqdm(configs, desc=file.stem):
                bucket_name = config["bucket_name"]

                for item in config["items"]:
                    try:
                        storage_uri = item["storage_uri"]
                        obj = client.get_object(bucket_name, storage_uri)
                        try:
                            img = imageio.imread(BytesIO(obj.data))
                            if img.shape[0] == 1:
                                continue
                        except Exception:
                            continue
                        objects.append((bucket_name, storage_uri))
                    except minio.S3Error:
                        pass
            print(f"Loaded: {len(objects)}/{len(configs)}")
        return objects

    _load_objects = _load_objects_v1

    def __getitem__(self, index):
        if self.memory_cache is not None and index in self.memory_cache:
            return self.memory_cache[index], 0

        bucket_name, path = self.objects[index]
        if self.cache_dir is not None:
            cur_cache_file: Path = self.cache_dir / bucket_name / path
            cur_cache_file.parent.mkdir(exist_ok=True, parents=True)
            if cur_cache_file.exists():
                data = cur_cache_file.read_bytes()
            else:
                client = minio.Minio(
                    MINIO_HOST,
                    access_key="root",
                    secret_key="hurh329iu43joi2njfds902139",
                    secure=MINIO_SECURE,
                )
                data = client.get_object(bucket_name, path).data
                cur_cache_file.write_bytes(data)
        else:
            client = minio.Minio(
                MINIO_HOST,
                access_key="root",
                secret_key="hurh329iu43joi2njfds902139",
                secure=MINIO_SECURE,
            )
            data = client.get_object(bucket_name, path).data

        image = Image.open(BytesIO(data))

        # Transforms で変換する。
        if self.transform is not None:
            image = self.transform(image)

        if self.memory_cache is not None:
            self.memory_cache[index] = image

        return image, 0

    def __len__(self):
        # ディレクトリ内の画像枚数を返す。
        return len(self.objects)


class ReshapeTransform:
    def __init__(self, new_size):
        self.new_size = new_size

    def __call__(self, img):
        return torch.reshape(img, self.new_size)


class CropTransform:
    def __init__(self, bbox):
        self.bbox = bbox

    def __call__(self, img):
        return img.crop(self.bbox)


class NormalizeShapeTransform:
    def __init__(self, shape2d=(1024, 1024)) -> None:
        self.shape = shape2d

    def __call__(self, img: Image) -> torch.Tensor:
        w, h = img.size
        if h > w:
            size = (h - w) // 2
            padded = TF.pad(img, (size, 0, h - w - size, 0), fill=0)
            return TF.resize(padded, self.shape)
        elif h < w:
            size = (w - h) // 2
            padded = TF.pad(img, (0, size, 0, w - h - size), fill=0)
            return TF.resize(padded, self.shape)
        else:
            return TF.resize(img, self.shape)


class FixedDepthTransform:
    def __init__(self, depth: int = 3):
        self.depth = depth

    def __call__(self, img: torch.Tensor) -> torch.Tensor:
        if img.shape[0] > self.depth:
            return img[:3, :, :]
        elif img.shape[0] < self.depth:
            return self(torch.cat([img] * (self.depth - img.shape[0] + 1), 0))
        else:
            return img


def get_train_data(conf):
    if conf.dataset.name == "cifar10":
        transform = T.Compose(
            [
                T.RandomHorizontalFlip(),
                T.ToTensor(),
                T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5), inplace=True),
            ]
        )
        transform_test = T.Compose(
            [
                T.ToTensor(),
                T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5), inplace=True),
            ]
        )

        train_set = torchvision.datasets.CIFAR10(
            conf.dataset.path, train=True, transform=transform, download=True
        )
        valid_set = torchvision.datasets.CIFAR10(
            conf.dataset.path, train=True, transform=transform_test, download=True
        )

        num_train = len(train_set)
        indices = torch.randperm(num_train).tolist()
        valid_size = int(np.floor(0.05 * num_train))

        train_idx, valid_idx = indices[valid_size:], indices[:valid_size]

        train_set = Subset(train_set, train_idx)
        valid_set = Subset(valid_set, valid_idx)

    elif conf.dataset.name == "svhn":
        transform = T.Compose(
            [
                T.ToTensor(),
                T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5), inplace=True),
            ]
        )
        transform_test = T.Compose(
            [
                T.ToTensor(),
                T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5), inplace=True),
            ]
        )

        train_set = torchvision.datasets.SVHN(
            conf.dataset.path, split="train", transform=transform, download=True
        )
        valid_set = torchvision.datasets.SVHN(
            conf.dataset.path, split="train", transform=transform_test, download=True
        )

        num_train = len(train_set)
        indices = torch.randperm(num_train).tolist()
        valid_size = int(np.floor(0.05 * num_train))

        train_idx, valid_idx = indices[valid_size:], indices[:valid_size]

        train_set = Subset(train_set, train_idx)
        valid_set = Subset(valid_set, valid_idx)

    elif conf.dataset.name == "celeba":
        transform = T.Compose(
            [
                CropTransform((25, 50, 25 + 128, 50 + 128)),
                T.Resize(128),
                T.RandomHorizontalFlip(),
                T.ToTensor(),
                T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5), inplace=True),
            ]
        )
        transform_test = T.Compose(
            [
                CropTransform((25, 50, 25 + 128, 50 + 128)),
                T.Resize(128),
                T.ToTensor(),
                T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5), inplace=True),
            ]
        )

        train_set = torchvision.datasets.CelebA(
            conf.dataset.path, split="train", transform=transform, download=True
        )
        valid_set = torchvision.datasets.CelebA(
            conf.dataset.path, split="train", transform=transform_test, download=True
        )

        num_train = len(train_set)
        indices = torch.randperm(num_train).tolist()
        valid_size = int(np.floor(0.05 * num_train))

        train_idx, valid_idx = indices[valid_size:], indices[:valid_size]

        train_set = Subset(train_set, train_idx)
        valid_set = Subset(valid_set, valid_idx)
    elif conf.dataset.name == "adult":
        transform_train = T.Compose(
            [
                NormalizeShapeTransform(
                    (conf.dataset.resolution, conf.dataset.resolution)
                ),
                T.RandomHorizontalFlip(),
                # T.RandomRotation(5),
                T.ToTensor(),
                FixedDepthTransform(),
                T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5), inplace=True),
            ],
        )
        transform_test = T.Compose(
            [
                NormalizeShapeTransform(
                    (conf.dataset.resolution, conf.dataset.resolution)
                ),
                T.ToTensor(),
                FixedDepthTransform(),
                T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5), inplace=True),
            ],
        )

        train_set = AdultImageLoader(
            split="train",
            transform=transform_train,
        )
        valid_set = AdultImageLoader(
            split="valid",
            transform=transform_test,
        )

    else:
        raise FileNotFoundError

    return train_set, valid_set
