wget -O mini.sh https://repo.anaconda.com/miniconda/Miniconda3-py38_4.12.0-Linux-x86_64.sh
chmod +x mini.sh
bash ./mini.sh -b -f -p /usr/local
pip install Pillow numpy matplotlib pytorch-lightning scikit-learn imageio minio
pip install --extra-index-url https://download.pytorch.org/whl/cu112/ torch torchvision